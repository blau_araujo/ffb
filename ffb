#!/usr/bin/env bash
# --------------------------------------------------------------------
# Script   : ffb
# Descrição: Fuzzy File Browser
# Versão   : 0.0.1
# Data     : 03/10/2020
# Licença  : GNU/GPL v3.0
# --------------------------------------------------------------------
# Copyright (C) 2020  Blau Araujo <blau@debxp.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------
# Uso: ffb [opções]
#
# Opções:
#
#     fzf|z         Utiliza o fzf (padrão)
#     rofi|r        Utiliza o Rofi
#     dmenu|d       Utiliza o dmenu
#
#     finder|f      Abre no modo buscador
#     browser|b     Abre no modo navegador (padrão)
#
#     help|h        Ajuda
#
# Modos:
#
#     browser (navegador) Navega sequencialmente pelas pastas
#                         até encontrar o arquivo a ser aberto.
#
#     finder (buscador)   Determina a pasta inicial da navegação
#                         a partir de uma busca em vários níveis
#                         de profundidade (padrão: 5 níveis).
# --------------------------------------------------------------------

# Settings -----------------------------------------------------------

if [[ -f /etc/ffbrc ]]; then
    . /etc/ffbrc
elif [[ -f $HOME/.ffbrc ]]; then
    . $HOME/.ffbrc
elif [[ -f $HOME/.config/ffb/config ]]; then
    . $HOME/.config/ffb/config
fi

# Globals ------------------------------------------------------------

STR_HELP='
Fuzzy File Browser
Navegador e buscador de arquivos utilizando menus "fuzzy".

Uso: ffb [opções]

Opções:

    fzf|z         Utiliza o fzf (padrão)
    rofi|r        Utiliza o Rofi
    dmenu|d       Utiliza o dmenu
    finder|f      Abre no modo buscador
    browser|b     Abre no modo navegador (padrão)
    help|h        Ajuda

Modos:

    browser (navegador) Navega sequencialmente pelas pastas
                        até encontrar o arquivo a ser aberto.

    finder (buscador)   Determina a pasta inicial da navegação
                        a partir de uma busca em vários níveis
                        de profundidade (padrão: 5 níveis).
'

STR_ERROR='Opção inválida!'

STR_FIND='Navegar em '

# Variável de controle do caminho atual...
CURRENT_DIR=$START_DIR

# Completa strings do menu de opções...
MENU_UP="${ICON_UP:-$FB_UP}  $STR_UP"
MENU_OPEN="${ICON_OPEN:-$FB_OPEN}  $STR_OPEN"
MENU_HIDDEN="${ICON_HIDDEN:-$FB_HIDDEN}  $STR_HIDDEN"
MENU_CLOSE="${ICON_CLOSE:-$FB_COLSE}  $STR_CLOSE"

# Comando do menu utilizado (rofi, dmenu, fzf, etc)...
MENU_CMD_ROFI='rofi -dmenu -i -p '
MENU_CMD_DMENU='dmenu -i -l 15 -p '
MENU_CMD_FZF='fzf --reverse -e -i --tiebreak=begin --prompt='

# Default options ----------------------------------------------------

MENU_CMD=$MENU_CMD_FZF
NAV_MODE='browser'

# Functions ----------------------------------------------------------

# Lista conteúdo do diretório...
# Uso: ffb_dir_list DIR
ffb_dir_list() {

    local path=$1
    local name menu f dl fl

    for f in $path/*; do
        [[ -d $f ]] && dl+="${ICON_DIR:-$FB_DIR}  ${f##*/}\n"
        [[ -f $f ]] && fl+="${ICON_FILE:-$FB_FILE}  ${f##*/}\n"
    done

    $SHOW_HIDDEN && {

        for f in $path/.*; do
            if [[ -d $f ]]; then
                [[ $f =~ ^.*/\.+$ ]] || dl+="${ICON_DIR:-$FB_DIR}  ${f##*/}\n"
            elif [[ -f $f ]]; then
                fl+="${ICON_FILE:-$FB_FILE}  ${f##*/}\n"
            fi
        done

    }

    menu="$MENU_OPEN\n$MENU_UP\n$MENU_HIDDEN\n$MENU_CLOSE\n"

    printf "$menu$dl$fl"
}

# Captura a seleção feita no menu...
get_selection() {

    local s

    s=$(awk '{ $1 = ""; sub(/^[ ]+/, "", $0); print }' <<< $@)

    case "$s" in
        "$STR_HIDDEN") s='toggle_hidden';;
            "$STR_UP") s='go_up';;
          "$STR_OPEN") s='open_dir';;
         "$STR_CLOSE") s='ffb_quit';;
    esac

    printf "$s"
}

# Alterna exibição de arquivos ocultos...
toggle_global_hidden() {

    $SHOW_HIDDEN && {
        SHOW_HIDDEN='false'
    } || {
        SHOW_HIDDEN='true'
    }
}

# Navega pelas pastas...
ffb_browser() {

    local menu_prompt s

    while :; do
        menu_prompt=" ${CURRENT_DIR:-/} "
        s="$(get_selection $($MENU_CMD"$menu_prompt" <<< $(ffb_dir_list $CURRENT_DIR)))"
        if [[ $s ]]; then
            [[ -e "$CURRENT_DIR/$s" ]] && s=$CURRENT_DIR/$s
            case "$s" in
                'open_dir')
                    xdg-open "$CURRENT_DIR" &> : &
                    exit 0
                    ;;
                'go_up')
                    CURRENT_DIR=${CURRENT_DIR%/*}
                    ;;
                'toggle_hidden')
                    toggle_global_hidden
                    ;;
                'quit')
                    exit 0
                    ;;
                *)
                    if [[ -d "$s" ]]; then
                        CURRENT_DIR="$s"
                    else
                        xdg-open "$s" &> : &
                        exit 0
                    fi
                    ;;
            esac
        else
            exit 0
        fi
    done
}

ffb_finder() {

    local s d f menu_prompt

    menu_prompt=" $STR_FIND${CURRENT_DIR:-/} "

    d=$(
        find $CURRENT_DIR -maxdepth $MAX_DEPTH -type d 2> /dev/null | \
        sed 's|^'$CURRENT_DIR'||; s/^\///; /^[[:space:]]*$/d' | \
        sort | \
        $MENU_CMD"$menu_prompt"
    )

    if [[ $d ]]; then
        CURRENT_DIR=$CURRENT_DIR/$d
        ffb_browser
    fi
}

get_options() {
    local p

    for p in "$@"; do

        if [[ -a $p ]]; then
            [[ ${p: -1} == '/' ]] && p=${p::-1}
            CURRENT_DIR=$p
            continue
        fi

        case $p in
               'rofi'|'r') MENU_CMD=$MENU_CMD_ROFI
                         ;;
              'dmenu'|'d') MENU_CMD=$MENU_CMD_DMENU
                         ;;
                'fzf'|'z') MENU_CMD=$MENU_CMD_FZF
                         ;;
            'browser'|'b') NAV_MODE='browser'
                         ;;
             'finder'|'f') NAV_MODE='finder'
                         ;;
               'help'|'h') show_help && exit 0
                         ;;
                      *) show_help && echo "'$p' - $STR_ERROR" && exit 1
                         ;;
        esac
    done
}

show_help() {
    echo "$STR_HELP"
}

# Main ---------------------------------------------------------------

get_options "$@"

[[ $NAV_MODE == 'browser' ]] && ffb_browser
[[ $NAV_MODE == 'finder' ]] && ffb_finder

