# ffb - Fuzzy File Browser

Navega por pastas uso com menus "fuzzy", como o `dmenu`, o `rofi` e o `fzf`, e abre o arquivo selecionado.

![](screenshot.png)


## Dependências

Um desses três programas deve estar instalado:

* fzf (preferencial e padrão no código)
* dmenu
* rofi

## Instalação

Clone o reposítório (ou baixe os arquivos) e copie o script `ffb` para uma pasta que esteja nas definições do seu `PATH`. O arquivo de configurações pessoais (`ffbrc`) deve ser copiado para um dos seguintes locais e com os seguintes nomes:

* `~/.ffbrc`
* `~/.config/ffb/config`
* `/etc/ffbrc`

## Uso

```
ffb [opções]

Opções:

    fzf|z         Utiliza o fzf (padrão)
    rofi|r        Utiliza o Rofi
    dmenu|d       Utiliza o dmenu
    finder|f      Abre no modo buscador
    browser|b     Abre no modo navegador (padrão)
    help|h        Ajuda

Modos:

    browser (navegador) Navega sequencialmente pelas pastas
                        até encontrar o arquivo a ser aberto.

    finder (buscador)   Determina a pasta inicial da navegação
                        a partir de uma busca em vários níveis
                        de profundidade (padrão: 5 níveis).
```